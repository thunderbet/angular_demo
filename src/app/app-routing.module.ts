import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UmlVisualizerComponent} from './uml-visualizer/uml-visualizer.component';


const routes: Routes = [ { path: 'uml-visualizer', component: UmlVisualizerComponent },];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
