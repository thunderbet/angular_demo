import {Component, OnInit} from '@angular/core';
import {SourceParserService} from '../services/source-parser.service';
import ts, {SourceFile} from 'typescript';
import {ClassModel} from '../models/classModel';

@Component({
  selector: 'app-uml-visualizer',
  templateUrl: './uml-visualizer.component.html',
  styleUrls: ['./uml-visualizer.component.scss']
})
export class UmlVisualizerComponent implements OnInit {

  classModel: ClassModel;
  fileName: string;

  constructor(private sourceParserService: SourceParserService) {
    this.classModel = new ClassModel('');
    this.fileName = 'person.ts';
  }

  ngOnInit() {
    // this.updateVisualizer();
  }

  /**
   * Calls the service to get the file content then parses it and displays it
   */
  updateVisualizer() {
    this.sourceParserService.getFileContent(this.fileName).subscribe(data => this.parseFileContent(data));
  }

  /**
   * Parses the file content and updates the classModel
   *
   * @param source
   */
  parseFileContent(source) {
    let sourceFile: SourceFile;

    // Parsing the content to into a SourceFile
    try {
      sourceFile = ts.createSourceFile(
        'file.ts',   // fileName
        source, // sourceText
        ts.ScriptTarget.Latest // langugeVersion
      );

      let statement;

      // Finding the statement that represents the body class and not the imports statements
      sourceFile.statements.forEach(stat => {
        // @ts-ignore
        if (stat.name !== undefined && stat.members !== undefined) {
          statement = stat;
          return;
        }
      });

      // Getting the class name
      // @ts-ignore
      this.classModel = new ClassModel(statement.name.text);

      // Getting the attributes and the methods of the class
      // @ts-ignore
      statement.forEachChild(child => {
          // @ts-ignore
          if (undefined !== child.name) {
            // @ts-ignore
            if (child.body !== undefined) {
              // @ts-ignore
              this.classModel.getMethods().push(child.name.text);
            } else {
              // @ts-ignore
              this.classModel.getAttributes().push(child.name.text);
            }
          }
        }
      );
    } catch (e) {
      alert('An error has occurred while parsing ' + this.fileName + ' : ' + e);
    }
  }
}
