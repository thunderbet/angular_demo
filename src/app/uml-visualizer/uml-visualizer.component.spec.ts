import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UmlVisualizerComponent } from './uml-visualizer.component';

describe('UmlVisualizerComponent', () => {
  let component: UmlVisualizerComponent;
  let fixture: ComponentFixture<UmlVisualizerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UmlVisualizerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UmlVisualizerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
