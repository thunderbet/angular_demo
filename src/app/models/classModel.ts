export class ClassModel {

  private className: string;
  private attributes: string[];
  private methods: string[];

  constructor(className: string) {
    this.className = className;
    this.attributes = [];
    this.methods = [];
  }


  getClassName(): string {
    return this.className;
  }

  setClassName(value: string) {
    this.className = value;
  }

  getAttributes(): string[] {
    return this.attributes;
  }

  setAttributes(value: string[]) {
    this.attributes = value;
  }

  getMethods(): string[] {
    return this.methods;
  }

  setMethods(value: string[]) {
    this.methods = value;
  }
}
