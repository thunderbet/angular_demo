import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UmlCardComponent } from './uml-card.component';

describe('UmlCardComponent', () => {
  let component: UmlCardComponent;
  let fixture: ComponentFixture<UmlCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UmlCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UmlCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
