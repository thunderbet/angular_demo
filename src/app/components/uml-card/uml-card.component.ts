import {Component, Input, OnInit} from '@angular/core';
import {ClassModel} from "../../models/classModel";

@Component({
  selector: 'app-uml-card',
  templateUrl: './uml-card.component.html',
  styleUrls: ['./uml-card.component.scss']
})
export class UmlCardComponent implements OnInit {

  @Input() classModel: ClassModel;

  constructor() { }

  ngOnInit() {
  }

}
