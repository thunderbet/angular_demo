import { TestBed } from '@angular/core/testing';

import { SourceParserService } from './source-parser.service';

describe('SourceParserService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SourceParserService = TestBed.get(SourceParserService);
    expect(service).toBeTruthy();
  });
});
