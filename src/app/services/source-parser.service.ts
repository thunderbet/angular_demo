import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SourceParserService {

  constructor(private httpClient: HttpClient) { }

  getFileContent(fileName: string): Observable<string> {
    return this.httpClient.get('assets/' + fileName, {responseType: 'text'});
  }
}
