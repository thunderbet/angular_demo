# Angular demo

## Implementation

J'ai décidé d'utiliser la classe `SourceFile` du module Typescript pour parser le code de la classe Typescript.
Il est possible aussi de faire la même chose via des regex, mais pourquoi réinventer la roue.
De plus `SourceFile` est flexible et convient tout à fait au besoin de cet exercice.

## Modelisation

Uml-Visualizer : component parent 

Uml-Card : component bête affichant un diagramme UML

ClassModel : modèle représentant une classe avec le nom de la classe, une liste d'attributs et de méthodes

Source-Parser : service qui "lit" un fichier source présent dans le dossier `assets`

## Packages utilisés

Angular 8

Angular Material

Flex Layout (bootstrap like)


## Test de la solution

'ng serve'

http://localhost:4200/uml-visualizer

Vous avez le fichier `person.ts` et `movie.ts` dans `assets` pour tester.

## Améliorations possibles

L'utilisation d'un file uploader pour charger le fichier à modéliser serait une bonne idée  selon moi.

